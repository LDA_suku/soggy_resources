#!/usr/bin/env python

import json
import os

for root, _dirnames, filenames in os.walk("binoutput"):
	for filename in filenames:
		if filename.endswith(".json"):
			fullpath = os.path.join(root, filename)
			with open(fullpath, "r+") as f:
				obj = json.load(f)
				f.seek(0, os.SEEK_SET)
				f.truncate()
				json.dump(obj, f, indent="\t")
				f.write("\n")
